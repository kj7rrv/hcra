# hcra
HamClock Remote Access - access HamClock remotely

See the client and server READMEs for more information.

## Client
The client is in the `client` directory.

## Server
The server is in the `server` directory.

## Thanks
* Elwood Downey WB0OEW for [HamClock](https://clearskyinstitute.com/ham/HamClock/)
